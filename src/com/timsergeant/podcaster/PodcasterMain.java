package com.timsergeant.podcaster;

import javax.swing.*;

/**
 * @author Tim Sergeant
 */
public class PodcasterMain {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Podcaster");
        PodcasterWindow podcasterWindow = new PodcasterWindow();

        frame.setContentPane(podcasterWindow.getContentPane());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
