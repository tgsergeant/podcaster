package com.timsergeant.podcaster;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tim Sergeant
 */
public class Podcast {

    private String name;
    private String description;
    private URL iconURL;
    private URL feedURL;
    private List<Episode> episodes;

    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("name", name);
        result.put("description", description);
        result.put("icon", iconURL);
        result.put("url", feedURL);

        JSONArray episodeArray = new JSONArray();
        for (Episode e : episodes) {
            episodeArray.put(e.toJSON());
        }
        result.put("episodes", episodeArray);

        return result;
    }

    public static Podcast fromJSON(JSONObject json) throws JSONException {
        Podcast result = new Podcast();

        result.name = json.getString("name");
        result.description = json.getString("description");
        try {
            result.feedURL = new URL(json.getString("url"));
        } catch (MalformedURLException e) {

        }
        try {
            result.iconURL = new URL(json.getString("icon"));
        } catch (MalformedURLException e) {

        }

        JSONArray episodeArray = json.getJSONArray("episodes");
        result.episodes = new ArrayList<Episode>(episodeArray.length());
        for (int i = 0; i < episodeArray.length(); i++) {
            result.episodes.add(Episode.fromJSON(episodeArray.getJSONObject(i)));
        }

        return result;
    }
}
