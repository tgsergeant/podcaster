package com.timsergeant.podcaster;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tim Sergeant
 */
public class Episode {

    private String title;
    private Date postDate;
    private String description;
    private long length;
    private URL downloadURL;

    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("title", title);
        result.put("date", postDate);
        result.put("description", description);
        result.put("length", length);
        result.put("url", downloadURL.toString());

        return result;
    }

    public static Episode fromJSON(JSONObject json) throws JSONException {
        Episode result = new Episode();

        result.title = json.getString("title");
        try {
            result.postDate = new SimpleDateFormat().parse(json.getString("date"));
        } catch (ParseException e) {
            result.postDate = new Date();
        }
        result.description = json.getString("description");
        result.length = json.getLong("length");
        try {
            result.downloadURL = new URL(json.getString("url"));
        } catch (MalformedURLException e) {

        }

        return result;
    }

}
