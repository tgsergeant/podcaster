package com.timsergeant.podcaster;

import javax.swing.*;

/**
 * @author Tim Sergeant
 */
public class PodcasterWindow {
    private JPanel contentPane;

    private JList feedList;
    private JButton addButton;
    private JButton syncButton;
    private JButton unsubscribeButton;
    private JProgressBar progressBar1;
    private JTable episodeTable;


    public JPanel getContentPane() {
        return contentPane;
    }

    private void createUIComponents() {
        // TODO
    }
}
