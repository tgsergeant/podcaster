Podcaster
===

I like the iTunes podcast interface, but I'm not going to install iTunes on a Windows computer just for that, and it's no good at all on Linux. Podcaster is an attempt to create a barebones, multi-platform version of a podcast downloader.

Goals
---

* Multiplatform
* Robust enough to check the podcast feeds I use regularly, not necessarily all feeds
* Not a Podcast player, just a downloader

Compiling
---

UIs were built with IntelliJ IDEA 12, so you'll need that to compile it for yourself. All data is stored in a JSON file, parsed with the [json.org library](http://www.json.org/java/).

